up: docker-up
down: docker-down
restart: down docker-build up


docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

docker-logs:
	docker-compose logs

docker-logs-follow:
	docker-compose logs -f

composer-install:
	docker-compose run --rm php-test composer install

db-migrate:
	docker-compose exec php-test php artisan migrate

db-seed:
	php artisan db:seed --class=UserSeeder

client-build:
	docker-compose run --rm client npm run build
