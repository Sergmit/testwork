## Install

- Insert rows to /etc/hosts
- - 127.0.0.1   test.loc
- ```git clone https://gitlab.com/Sergmit/testwork.git```
- ```copy .env.example to .env```
- ```make build```
- ```make up```
- ```chmod -R 777 app/storage app/bootstrap/cache```
- ```make composer-install```
- ```make db-migrate```
- ```make db-seed```
- ```make client-install```
- ```make client-build```