<?php

namespace App\Data;

use Spatie\LaravelData\Data;

class FetchUserListData extends Data
{
    public function __construct(
        public int $page = 1,
    ) {}
}
