<?php

namespace App\Http\Controllers;

use App\Data\LoginData;
use App\Services\UserService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(LoginData $loginData, UserService $userService)
    {
        $userService->login($loginData);

        return response()->json(['success' => true]);
    }
}
