<?php

namespace App\Http\Controllers;

use App\Data\FetchUserListData;
use App\Data\UpdateUserData;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(FetchUserListData $fetchUserListData, UserRepository $userRepository): JsonResponse
    {
        $users = $userRepository->fetchUsers();

        return $users->response();
    }


    /**
     * @param UpdateUserData $updateUserData
     * @param UserService $userService
     * @param User $user
     * @return JsonResponse
     */
    public function update(UpdateUserData $updateUserData,
                           UserService $userService,
                           UserRepository $userRepository,
                           User $user): JsonResponse
    {
        $userService->updateUser($updateUserData, $user);
        $users = $userRepository->fetchUsers();

        return $users->response();
    }

    /**
     * @param UserRepository $userRepository
     * @param UserService $userService
     * @param User $user
     * @return JsonResponse
     */
    public function destroy(UserRepository $userRepository,
                            UserService $userService,
                            User $user): JsonResponse
    {
        $userService->deleteUser($user);
        $users = $userRepository->fetchUsers();

        return $users->response();
    }
}
