<?php

namespace App\Repositories;

use App\Data\FetchUserListData;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserRepository
{
    /**
     * @return AnonymousResourceCollection
     */
    public function fetchUsers(): AnonymousResourceCollection
    {
        return UserResource::collection(User::orderBy('id')->paginate(10));
    }

    /**
     * @param string $email
     * @return User
     */
    public function fetchUserByEmail(string $email): User
    {
        return User::where('email', $email)->first();
    }
}
