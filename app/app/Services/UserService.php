<?php

namespace App\Services;

use App\Data\LoginData;
use App\Data\UpdateUserData;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserService
{
    public function __construct(private UserRepository $userRepository)
    {
    }

    /**
     * @param UpdateUserData $userData
     * @param User $user
     * @return void
     */
    public function updateUser(UpdateUserData $userData, User $user): void
    {
        $user->name = $userData->name;
        $user->email = $userData->email;
        $user->save();
    }

    public function deleteUser(User $user)
    {
        if ($user->name !== 'admin') {
            $user->delete();
        }
    }

    /**
     * @param LoginData $loginData
     * @return JsonResponse
     */
    public function login(LoginData $loginData): JsonResponse
    {
        $user = $this->userRepository->fetchUserByEmail($loginData->email);
        if (!$user) {
            throw new BadRequestHttpException("Email is not found");
        }

        if (Hash::check($loginData->password, $user->password)) {
            Auth::login($user);
            return response()->json(['success' => true]);
        } else {
            throw new BadRequestHttpException("Bad password");
        }
    }
}
