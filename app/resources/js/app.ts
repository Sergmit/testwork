import { createApp } from "vue";
import LoginComponent from "./LoginComponent.vue";
import UsersComponent from "./UsersComponent.vue";
import "bootstrap/dist/js/bootstrap";

const app = createApp({});
import "bootstrap/dist/css/bootstrap.min.css";

app.component("login-component", LoginComponent);
app.component("users-component", UsersComponent);
app.config.globalProperties.$log = console.log;
app.mount("#app");
