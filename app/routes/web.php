<?php

use App\Http\Middleware\AuthMiddleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

Route::get('/users-view', function() {
   return view('users');
})->middleware(AuthMiddleware::class);

Route::resource(
    'users', UserController::class,
)->only(['index', 'update', 'destroy'])->middleware(['auth']);

Route::post('/login', [AuthController::class, 'login']);
